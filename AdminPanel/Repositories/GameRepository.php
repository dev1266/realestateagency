<?php

namespace AdminPanel\Repositories;

use AdminPanel\Core\Repository\BaseRepository;
use AdminPanel\Models\Game\Game;

/**
 * @method null|Game findById(int $id, ?array $columns = null)
 * @method array list()
 * @method bool create()
 * @method bool update(int $id)
 * @method bool delete(int $id)
 */
class GameRepository extends BaseRepository
{
    protected string $object = Game::class;
    protected string $table = 'Games';
}