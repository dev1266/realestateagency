<?php

namespace AdminPanel\Repositories;

use AdminPanel\Core\Repository\BaseRepository;
use AdminPanel\Models\Game\Character\CharacterStats;

/**
 * @method null|CharacterStats findById(int $id, ?array $columns = null)
 * @method array list()
 * @method CharacterStats create()
 * @method bool update(int $id)
 * @method bool delete(int $id)
 */
class StatsRepository extends BaseRepository
{
    protected string $object = CharacterStats::class;
    protected string $table = 'Stats';

}