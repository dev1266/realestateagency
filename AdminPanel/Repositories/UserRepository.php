<?php

namespace AdminPanel\Repositories;

use AdminPanel\Core\Repository\BaseRepository;
use AdminPanel\Models\User;

/**
 * @method null|User findById(int $id, ?array $columns = null)
 * @method array list()
 * @method bool create()
 * @method bool update(int $id)
 * @method bool delete(int $id)
 */
class UserRepository extends BaseRepository
{
    protected string $object = User::class;
    protected string $table = 'Users';

    /**
     * @param string|null $registrationDate
     * @return array|null
     */
    public function getRegistration(?string $registrationDate): ?array
    {
        $data = $this->fetch("
            SELECT 
                * 
            FROM {$this->table}
            WHERE CreatedDate >= $registrationDate
        ");

        if (!$data) {
            return null;
        }

        return $data;
    }
}