<?php

namespace AdminPanel\Repositories;

use AdminPanel\Core\Repository\BaseRepository;
use AdminPanel\Models\Game\Character\Character;

/**
 * @method null|Character findById(int $id, ?array $columns = null)
 * @method array list()
 * @method Character create()
 * @method bool update(int $id)
 * @method bool delete(int $id)
 */
class CharacterRepository extends BaseRepository
{
    protected string $object = Character::class;
    protected string $table = 'Characters';

}