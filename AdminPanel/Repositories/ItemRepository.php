<?php

namespace AdminPanel\Repositories;

use AdminPanel\Core\Repository\BaseRepository;
use AdminPanel\Models\Game\Items\Item;

/**
 * @method null|Item findById(int $id, ?array $columns = null)
 * @method array list()
 * @method Item create(Item $item)
 * @method bool update(int $id)
 * @method bool delete(int $id)
 */
class ItemRepository extends BaseRepository
{
    protected string $object = Item::class;
    protected string $table = 'Items';
}