<?php

namespace AdminPanel;

use AdminPanel\Core\System;


require_once __DIR__ . '/vendor/autoload.php';

$system = new System();
$system->run();