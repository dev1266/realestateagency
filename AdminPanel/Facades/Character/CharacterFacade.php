<?php

namespace AdminPanel\Facades\Character;

use AdminPanel\Models\Game\Character\Character;
use AdminPanel\Models\Game\Enums\CharacterClassesEnum;
use AdminPanel\Models\Game\Enums\RacesEnum;

class CharacterFacade extends AbstractCharacterFacade
{
    public function createNewCharacter(CharacterClassesEnum $class, RacesEnum $race, string $nickname, int $userId): Character
    {
        $characterClass = $this->selectCharacterClassService->selectClass($class, $race);

        $stats = $this->statsRepository->create($characterClass->getStartStats());

        $character = $this->characterRepository->create($nickname, $class, $stats->ID);

        $inventory = $this->inventoryService->createInventory($character);

        $character->Inventory = $inventory;

        return $character;
    }
}