<?php

namespace AdminPanel\Facades\Character;

use AdminPanel\Core\Facades\Facade;
use AdminPanel\Repositories\CharacterRepository;
use AdminPanel\Repositories\StatsRepository;
use AdminPanel\Services\Character\InventoryService;
use AdminPanel\Services\Character\CharacterClassService;

abstract class AbstractCharacterFacade implements Facade
{
    protected CharacterClassService $selectCharacterClassService;
    protected InventoryService $inventoryService;
    protected CharacterRepository $characterRepository;
    protected StatsRepository $statsRepository;

    public function __construct()
    {
        $this->selectCharacterClassService = new CharacterClassService();
        $this->characterRepository = new CharacterRepository();
        $this->statsRepository = new StatsRepository();
        $this->inventoryService = new InventoryService();
    }
}