<?php

namespace AdminPanel\Services\Character;

use AdminPanel\Models\Game\Character\CharacterClasses\AbstractCharacterClass;
use AdminPanel\Models\Game\Character\CharacterClasses\Factories\WarriorFactory;
use AdminPanel\Models\Game\Character\CharacterClasses\Factories\WizardFactory;
use AdminPanel\Models\Game\Enums\CharacterClassesEnum;
use AdminPanel\Models\Game\Enums\RacesEnum;

class CharacterClassService
{
    public function selectClass(CharacterClassesEnum $class, RacesEnum $race): AbstractCharacterClass
    {
        $factory = match ($class->name) {
            'Warrior' => new WarriorFactory(),
            'Wizard' => new WizardFactory(),
        };

        return match ($race->name) {
            'Dwarf' => $factory->createDwarf(),
            'Goblin' => $factory->createGoblin(),
            'Halfling' => $factory->createHalfling(),
        };
    }
}