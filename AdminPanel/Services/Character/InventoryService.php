<?php

namespace AdminPanel\Services\Character;

use AdminPanel\Models\Game\Character\Character;
use AdminPanel\Models\Game\Character\CharacterClasses\AbstractCharacterClass;
use AdminPanel\Models\Game\Character\CharacterClasses\Factories\WarriorFactory;
use AdminPanel\Models\Game\Character\CharacterClasses\Factories\WizardFactory;
use AdminPanel\Models\Game\Character\Inventory\Inventory;
use AdminPanel\Models\Game\Enums\CharacterClassesEnum;
use AdminPanel\Models\Game\Enums\RacesEnum;
use AdminPanel\Repositories\ItemRepository;

class InventoryService
{

    protected ItemRepository $itemRepository;

    public function __construct()
    {
        $this->itemRepository = new ItemRepository();
    }

    public function createInventory(Character $character): Inventory
    {
        $factory = match ($character->CharacterClass->name) {
            'Warrior' => new WarriorFactory(),
            'Wizard' => new WizardFactory(),
        };

        $inventory = new Inventory();
        $weapon = $factory->createWeapon();
        $weapon->UserID = $character->ID;
        $armor = $factory->createArmor();
        $armor->UserID = $character->ID;

        $inventory->add($this->itemRepository->create($weapon));
        $inventory->add( $this->itemRepository->create($armor));

        return $inventory;
    }

}