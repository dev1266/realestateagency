<?php

namespace AdminPanel\Models;

use AdminPanel\Core\Model\Model;

class User extends Model
{
    public string $ID;
    public string $NickName;
    public string $FirstName;
    public string $LastName;
    public string $Email;
    private string $CreatedDate;
    private string $UpdatedDate;


}