<?php

namespace AdminPanel\Models\Game\Items;

use AdminPanel\Core\Model\Model;

abstract class Item extends Model
{
    public int $ID;
    public int $UserID;

    public string $Name;
    public string $Type;

}