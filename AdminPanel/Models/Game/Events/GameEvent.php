<?php

namespace AdminPanel\Models\Game\Events;

use AdminPanel\Core\Model\Model;
use AdminPanel\Models\Game\Game;
use AdminPanel\Repositories\GameRepository;

class GameEvent extends Model
{
    public int $ID;
    public string $Name;
    public string $Description;
    public Game $Game;

    public function __construct(private int $GameID)
    {
        $this->Game = (new GameRepository())->findById($this->GameID);
    }
}