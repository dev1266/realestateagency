<?php

namespace AdminPanel\Models\Game\Character\Stats;

class CompositeStats extends BaseStats
{
    /**
     * @var array []BaseStats
     */
    protected array $listStats = [];

    public function add(BaseStats $stats): void
    {
        $this->listStats[] = $stats;
    }

    public function remove(string $classname): void
    {
        foreach ($this->listStats as &$stats) {
            if ($stats instanceof $classname) {
                unset($stats);
            }
        }
    }

    public function getStats(): array
    {
        $result = [];
        foreach ($this->listStats as $stats) {
            foreach ($stats->getStats() as $key => $value) {
                $result[$key] = $value;
            }
        }
        return $result;
    }


}