<?php

namespace AdminPanel\Models\Game\Character\Stats;

use AdminPanel\Core\Model\Model;

abstract class BaseStats extends Model
{
    public int $ID;

    public string $Name;
    public int $Health;
    public int $ManaPoint;
    public int $Endurance;
    public int $Strength;
    public int $Faith;
    public int $Intelligence;
    public int $Expireince;
    public int $Level;
    public int $Speed;
    public int $Gold;

    public function __construct()
    {
    }

    public function getStats(): array
    {
        return $this->toArray();
    }
}