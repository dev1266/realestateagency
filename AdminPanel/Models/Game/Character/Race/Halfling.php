<?php

namespace AdminPanel\Models\Game\Character\Race;

use AdminPanel\Models\Game\Character\Stats\Race\HalflingStats;

class Halfling extends BaseRace
{
    public function __construct()
    {
        $this->raceStats = new HalflingStats();
    }

}