<?php

namespace AdminPanel\Models\Game\Character\Race;

use AdminPanel\Models\Game\Character\Stats\BaseStats;

abstract class BaseRace
{
    protected BaseStats $raceStats;

    public function getDefaultStats(): BaseStats
    {
        return $this->raceStats;
    }
}