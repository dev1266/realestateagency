<?php

namespace AdminPanel\Models\Game\Character\Race;

use AdminPanel\Models\Game\Character\Stats\Race\DwarfStats;

class Dwarf extends BaseRace
{
    public function __construct()
    {
        $this->raceStats = new DwarfStats();
    }
}