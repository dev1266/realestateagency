<?php

namespace AdminPanel\Models\Game\Character\Race;

use AdminPanel\Models\Game\Character\Stats\Race\GoblinStats;

class Goblin extends BaseRace
{
    public function __construct()
    {
        $this->raceStats = new GoblinStats();
    }
}