<?php

namespace AdminPanel\Models\Game\Character;

use AdminPanel\Core\Model\Model;
use AdminPanel\Models\Game\Character\Inventory\Inventory;
use AdminPanel\Models\Game\Game;
use AdminPanel\Models\User;
use AdminPanel\Repositories\StatsRepository;
use AdminPanel\Repositories\UserRepository;

class Character extends Model
{
    public int $ID;
    public string $NickName;

    public Stats $Stats;
    public User $User;

    public Game $Game;
    public CharacterClass $CharacterClass;
    public Inventory $Inventory;


    public function __construct()
    {
    }

}