<?php

namespace AdminPanel\Models\Game\Character\CharacterClasses\Classes;

use AdminPanel\Models\Game\Character\CharacterClasses\AbstractCharacterClass;
use AdminPanel\Models\Game\Character\Race\BaseRace;
use AdminPanel\Models\Game\Character\Stats\Class\WizardStats;

class Wizard extends AbstractCharacterClass
{
    protected string $Name = 'Wizard';
    public function __construct(BaseRace $race)
    {
        parent::__construct($race);
        $this->classStats = new WizardStats();
        $this->compositeStats->add($this->classStats);
    }
}