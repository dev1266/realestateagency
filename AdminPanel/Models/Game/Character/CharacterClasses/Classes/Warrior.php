<?php

namespace AdminPanel\Models\Game\Character\CharacterClasses\Classes;

use AdminPanel\Models\Game\Character\CharacterClasses\AbstractCharacterClass;
use AdminPanel\Models\Game\Character\Race\BaseRace;
use AdminPanel\Models\Game\Character\Stats\Class\WarriorStats;

class Warrior extends AbstractCharacterClass
{
    protected string $Name = 'Warrior';
    public function __construct(BaseRace $race)
    {
        parent::__construct($race);
        $this->classStats = new WarriorStats();
        $this->compositeStats->add($this->classStats);
    }
}