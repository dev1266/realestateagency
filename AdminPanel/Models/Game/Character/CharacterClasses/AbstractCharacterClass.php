<?php

namespace AdminPanel\Models\Game\Character\CharacterClasses;

use AdminPanel\Core\Model\Model;
use AdminPanel\Models\Game\Character\Race\BaseRace;
use AdminPanel\Models\Game\Character\Stats\BaseStats;
use AdminPanel\Models\Game\Character\Stats\CompositeStats;

abstract class AbstractCharacterClass
{
    protected string $Name = "BaseCharacter";
    protected CompositeStats $compositeStats;

    protected BaseStats $classStats;

    public function __construct(protected BaseRace $race)
    {
        $this->compositeStats = new CompositeStats();
        $this->compositeStats->add($this->race->getDefaultStats());
    }

    public function getStartStats(): array
    {
        return $this->compositeStats->getStats();
    }


}