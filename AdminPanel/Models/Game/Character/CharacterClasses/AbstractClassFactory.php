<?php

namespace AdminPanel\Models\Game\Character\CharacterClasses;

use AdminPanel\Models\Game\Character\CharacterClasses\Classes\Warrior;

abstract class AbstractClassFactory
{
    abstract public function createDwarf();
    abstract public function createGoblin();
    abstract public function createHalfling();
    abstract public function createArmor();
    abstract public function createWeapon();

}