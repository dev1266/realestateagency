<?php

namespace AdminPanel\Models\Game\Character\CharacterClasses\Factories;

use AdminPanel\Models\Game\Character\CharacterClasses\AbstractClassFactory;
use AdminPanel\Models\Game\Character\CharacterClasses\Classes\Wizard;
use AdminPanel\Models\Game\Character\Race\Dwarf;
use AdminPanel\Models\Game\Character\Race\Goblin;
use AdminPanel\Models\Game\Character\Race\Halfling;
use AdminPanel\Models\Game\Items\Item;
use AdminPanel\Models\Game\Items\Weapon\Axe;
use AdminPanel\Models\Game\Items\Weapon\Stick;

class WizardFactory extends AbstractClassFactory
{
    public function createDwarf(): Wizard
    {
        return new Wizard(new Dwarf());
    }

    public function createGoblin(): Wizard
    {
        return new Wizard(new Goblin());
    }

    public function createHalfling(): Wizard
    {
        return new Wizard(new Halfling());
    }

    public function createArmor(): Item
    {
        return new Axe();
    }

    public function createWeapon(): Item
    {
        return new Stick();
    }
}