<?php

namespace AdminPanel\Models\Game\Character\CharacterClasses\Factories;

use AdminPanel\Models\Game\Character\CharacterClasses\AbstractClassFactory;
use AdminPanel\Models\Game\Character\CharacterClasses\Classes\Warrior;
use AdminPanel\Models\Game\Character\Race\Dwarf;
use AdminPanel\Models\Game\Character\Race\Goblin;
use AdminPanel\Models\Game\Character\Race\Halfling;
use AdminPanel\Models\Game\Items\Item;
use AdminPanel\Models\Game\Items\Weapon\Axe;

class WarriorFactory extends AbstractClassFactory
{
    public function createDwarf(): Warrior
    {
        return new Warrior(new Dwarf());
    }

    public function createGoblin(): Warrior
    {
        return new Warrior(new Goblin());
    }

    public function createHalfling(): Warrior
    {
        return new Warrior(new Halfling());
    }

    public function createArmor(): Item
    {
        return new Axe();
    }

    public function createWeapon(): Item
    {
        return new Axe();
    }
}