<?php

namespace AdminPanel\Models\Game\Character\Inventory;

use AdminPanel\Models\Game\Items\Item;

class Inventory extends BaseInventory
{
    /**
     * @var Item[]
     */
    private array $items = [];

    public function add(Item $item): void
    {
        if ($this->items[$item->ID]) {
            return;
        }
        $this->items[$item->ID] = $item;
    }

    public function get(int $id): ?Item
    {
        return $this->items[$id] ?? $this->itemRepository->findById($id);
    }

    public function remove(int $id): void
    {
        unset($this->items[$id]);
    }

}