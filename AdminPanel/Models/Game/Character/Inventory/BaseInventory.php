<?php

namespace AdminPanel\Models\Game\Character\Inventory;

use AdminPanel\Repositories\ItemRepository;

abstract class BaseInventory
{
    protected ItemRepository $itemRepository;
    public function __construct()
    {
        $this->itemRepository = new ItemRepository();
    }

}