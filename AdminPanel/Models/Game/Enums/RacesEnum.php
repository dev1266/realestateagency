<?php

namespace AdminPanel\Models\Game\Enums;

enum RacesEnum
{
    case Dwarf;
    case Goblin;
    case Halfling;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }
}
