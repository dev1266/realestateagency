<?php

namespace AdminPanel\Models\Game\Enums;

enum CharacterClassesEnum
{
    case Wizard;
    case Warrior;
}