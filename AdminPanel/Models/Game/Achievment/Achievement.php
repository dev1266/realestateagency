<?php

namespace AdminPanel\Models\Game\Achievment;

use AdminPanel\Core\Model\Model;
use AdminPanel\Models\Game\Character\Character;
use AdminPanel\Repositories\CharacterRepository;

class Achievement extends Model
{
    public int $ID;
    public string $Name;
    public Character $Character;

    public function __construct(public int $CharacterId)
    {
        $this->Character = (new CharacterRepository())->findById($this->CharacterId);
    }
}