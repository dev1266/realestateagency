<?php

namespace AdminPanel\Models\Game;

use AdminPanel\Core\Model\Model;

class Game extends Model
{
    public int $Id;
    public string $Name;
    public int $Price;
    public int $Genre;
}