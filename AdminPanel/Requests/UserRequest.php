<?php

namespace AdminPanel\Requests;

use AdminPanel\Core\Request\Request;

class UserRequest extends Request
{
    protected array $rules = [
        'ID' => ['required', 'int'],
        'Name' => ['required', 'string']
    ];
}