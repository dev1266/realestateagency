<?php

namespace AdminPanel\Requests;

use AdminPanel\Core\Request\Request;

class GameRequest extends Request
{
    protected array $rules = [
        'ID' => ['required', 'int'],
        'Name' => ['required', 'string']
    ];
}