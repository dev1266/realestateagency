<?php

namespace AdminPanel\Controllers\Crud;

use AdminPanel\Core\Controllers\CrudController\CrudController;

/**
 * @property \AdminPanel\Requests\CharacterRequest $request
 * @property \AdminPanel\Facades\Character\CharacterFacade $facade
 */
class CharacterController extends CrudController
{

    public function list(): array
    {
        // TODO: Implement list() method.
    }

    public function info(int $id): array
    {
        // TODO: Implement info() method.
    }

    public function create(): array
    {
        if (!$this->request->validate()) {
            return [];
        }

        $class = $this->request->get('class', 0);
        $race = $this->request->get('race', 0);
        $nickname = $this->request->get('class', 0);
        $userID = $this->request->get('userId', null);

        $character = $this->facade->createNewCharacter($class, $race, $nickname, $userID);

        return $character;
    }

    public function update(): array
    {
        // TODO: Implement update() method.
    }

    public function delete(int $id): bool
    {
        // TODO: Implement delete() method.
    }
}