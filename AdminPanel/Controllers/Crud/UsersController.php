<?php

namespace AdminPanel\Controllers\Crud;

use AdminPanel\Core\Controllers\CrudController\CrudController;

/**
 * @property \AdminPanel\Requests\UserRequest $request
 * @property \AdminPanel\Repositories\UserRepository $repository
 */
class UsersController extends CrudController
{
    /**
     * @return array
     */
    public function list(): array
    {
        echo 'Users';
        return [];
    }

    /**
     * @param int $id
     * @return array
     */
    public function info(int $id): array
    {
        $user = $this->repository->findById($id);

        var_dump($user);

        if (is_null($user)) {
            return [];
        }

        echo 'User: '. $user->ID;
        echo 'User: '. $user->FirstName;


        return $user->toArray();
    }

    /**
     * @return array|string[]
     */
    public function create(): array
    {
        if (!$this->request->validate()) {
            return ['Error'];
        }

        echo 'Create';
        return [];
    }

    /**
     * @return array|string[]
     */
    public function update(): array
    {
        if (!$this->request->validate()) {
            return ['Error'];
        }

        echo 'Update';
        return [];
    }

    /**
     * @param int $id
     * @return bool
     */
    public function delete(int $id): bool
    {
        echo 'Delete: ' . $id;
        return true;
    }
}