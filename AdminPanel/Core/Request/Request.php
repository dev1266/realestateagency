<?php

namespace AdminPanel\Core\Request;

use AdminPanel\Core\Validation\Validator;

abstract class Request
{
    protected array $rules = [];
    private Validator $validator;
    public function __construct(protected array $parameters = [])
    {
        $this->validator = new Validator($this->rules, $this->parameters);
    }

    public function validate(): bool
    {
        return $this->validator->validate();
    }

    public function get(string $key, mixed $default): mixed
    {
        return $this->parameters[$key] ?? $default;
    }

    public function getAll(): array
    {
        return $this->parameters;
    }

}