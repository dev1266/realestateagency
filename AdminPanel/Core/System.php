<?php

namespace AdminPanel\Core;

use AdminPanel\Controllers\Crud\CharacterController;
use AdminPanel\Controllers\Crud\GamesController;
use AdminPanel\Controllers\Crud\UsersController;
use AdminPanel\Core\Controllers\CrudController\CrudController;
use AdminPanel\Facades\Character\CharacterFacade;
use AdminPanel\Models\Game\Character\Character;
use AdminPanel\Repositories\CharacterRepository;
use AdminPanel\Repositories\UserRepository;
use AdminPanel\Requests\CharacterRequest;
use AdminPanel\Requests\UserRequest;

class System
{
    private array $url = [];
    public function __construct()
    {
        if (!empty($_SERVER['REQUEST_URI'])) {
            $this->url = explode("/", $_SERVER['REQUEST_URI']);
        }
    }

    public function run(): void
    {
        echo $this->url[1];
        $controller = match ($this->url[1]) {
            'Users' => new UsersController(new UserRequest($_POST['User'] ?? []), new UserRepository()),
            'Games' => new GamesController(new UserRequest($_POST['Game'] ?? []), new UserRepository()),
            'Characters' => new CharacterController(
                new CharacterRequest($_POST['Character'] ?? []),
                new CharacterRepository(),
                new CharacterFacade()
            )
        };

        if ($controller instanceof CrudController) {
            match ($this->url[2]) {
                'list' => $controller->list(),
                'info' => $controller->info($this->url[3]),
                'create' => $controller->create(),
                'update' => $controller->update(),
                'delete' => $controller->delete($this->url[3]),
            };
        }
    }
}