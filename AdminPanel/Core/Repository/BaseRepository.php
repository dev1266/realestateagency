<?php

namespace AdminPanel\Core\Repository;

class BaseRepository extends Repository implements IRepository
{
    public function findById(int $id, ?array $columns = null)
    {
        $select = $columns ? implode(',', $columns) : '*';

        $query = "SELECT $select FROM {$this->table} WHERE ID = $id ORDER BY ID";

        $user = $this->fetchObject($query);

        if (!$user) {
            return null;
        }

        return $user;
    }

    public function list(): array
    {
        // TODO: Implement list() method.
    }

    public function update(int $id): bool
    {
        // TODO: Implement update() method.
    }

    public function create()
    {
        // TODO: Implement create() method.
    }

    public function delete(int $id): bool
    {
        // TODO: Implement delete() method.
    }
}