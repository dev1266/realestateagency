<?php

namespace AdminPanel\Core\Repository;

interface IRepository
{
    public function findById(int $id,  ?array $columns = null);
    public function list(): array;
    public function update(int $id): bool;
    public function create();
    public function delete(int $id): bool;
}