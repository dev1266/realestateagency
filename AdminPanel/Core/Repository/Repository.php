<?php

namespace AdminPanel\Core\Repository;

use AdminPanel\Core\DB;
use PDO;

abstract class Repository
{
    protected string $object;
    protected string $table;
    /**
     * @var \AdminPanel\Core\DB
     */
    private DB $connection;

    public function __construct()
    {
        $this->connection = new DB();
    }

    protected function fetch(string $query): ?array
    {
        $statement = $this->connection->query($query);

        if (!$statement) {
            return null;
        }

        $array = $statement->fetch(PDO::FETCH_ASSOC);

        if (!$array) {
            return null;
        }

        return $array;
    }

    protected function fetchObject(string $query)
    {
        $statement = $this->connection->query($query);

        if (!$statement) {
            return null;
        }

        $object = $statement->fetchObject($this->object);

        if (!$object) {
            return null;
        }

        return $object;
    }
}