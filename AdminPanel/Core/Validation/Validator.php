<?php

namespace AdminPanel\Core\Validation;

class Validator
{
    public function __construct(private array $rules, private array $parameters)
    {
    }

    public function validate(): bool
    {
        $result = true;
        foreach ($this->rules as $key => $fieldRules) {
            foreach ($fieldRules as $rule) {
                $value = $this->parameters[$key];
                $result = match ($rule) {
                    'required' => isset($this->parameters[$key]),
                    'int' => !isset($this->parameters[$key]) || is_int($value) || is_numeric($value),
                    'string' => !isset($this->parameters[$key]) || is_string($value)
                };
            }
        }

        return $result;
    }
}