<?php

namespace AdminPanel\Core;

use PDO;

class DB extends PDO
{
    private const DRIVER = 'mysql';
    private const USER = 'test';
    private const PASS = 'test';
    private const HOST = 'mysql';
    private const PORT = '3306';
    private const DATABASE = 'test';

    public function __construct()
    {
        $dns = self::DRIVER .
            ':host=' . self::HOST .
            ';port=' . self::PORT .
            ';dbname=' . self::DATABASE;

        parent::__construct($dns, self::USER, self::PASS);
    }
}