<?php

namespace AdminPanel\Core\Controllers\CrudController;

use AdminPanel\Core\Facades\Facade;
use AdminPanel\Core\Repository\Repository;
use AdminPanel\Core\Request\Request;

abstract class CrudController implements CrudControllerInterface
{
    public function __construct(
        public Request $request,
        public Repository $repository,
        public ?Facade $facade = null
    ) {}
}