<?php

namespace AdminPanel\Core\Controllers\CrudController;

use AdminPanel\Core\Controllers\Controller;

interface CrudControllerInterface extends Controller
{
    public function list(): array;
    public function info(int $id): array;
    public function create(): array;
    public function update(): array;
    public function delete(int $id): bool;
}